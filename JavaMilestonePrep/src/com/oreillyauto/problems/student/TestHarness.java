package com.oreillyauto.problems.student;

/**
 * - Build a class called Student.java
 * - DO NOT use an access modifier for your class!
 * - The test harness below creates new students.
 * - Your class implementation should keep a running total of all students created.
 * - The member variable that holds the total of all students should be called "counter"
 * - Once complete, the test harness should have access to your member variable (counter)
 * - If the test harness count matches your Student count, then you pass. 
 * 
 * @author jbrannon5
 *
 */
public class TestHarness {   
    
    public static void main(String[] args) {
        int numberOfStudents = 4; 

        for (int i = 0; i < numberOfStudents; i++) {
            new Student("Bob");
        }
        
        if (numberOfStudents == Student.counter) {
            System.out.println("Pass!!!");
        } else {
            System.out.println("Fail!!!");
            System.out.println("Expected count="+numberOfStudents + 
                               ". Your count is " + Student.counter);
        }
    }
}