package com.oreillyauto.problems.volume;

class VolumeImplementation extends Volume{
    int gallons;
    float ounces;
    
    @Override
    void setGallonsAndOunces(int gallons, float ounces) {
        this.gallons = gallons;
        this.ounces = ounces;
        
    }

    @Override
    int getGallons() {
        // TODO Auto-generated method stub
        return this.gallons;
    }

    @Override
    float getOunces() {
        // TODO Auto-generated method stub
        return this.ounces;
    }

    @Override
    String getVolumeComparison(Volume volume) {
        // TODO Auto-generated method stub
        float thisGallon = getGallons();
        float ounces = getOunces();
        thisGallon += (ounces/128);
        
        float thatGallon = volume.getGallons();
        float thatOunces = volume.getOunces();
        thatGallon += (thatOunces/128);
        
        if (thisGallon > thatGallon) {
            return "First volume is greater.";
        } else {
            return "Second volume is greater.";
        }
    }
    
}