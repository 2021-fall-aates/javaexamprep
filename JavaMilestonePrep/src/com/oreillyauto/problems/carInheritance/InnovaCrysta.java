package com.oreillyauto.problems.carInheritance;

class InnovaCrysta extends Car{
    
    protected int milage;
    private final static boolean isSedan = false;
    private final static String seats = "6";
    
    

    public InnovaCrysta(int milage) {
        super(isSedan, seats);
        this.milage = milage;
    }

    public InnovaCrysta(boolean isSedan, String seats, int milage) {
        super(isSedan, seats);
        this.milage = milage;
    }
    
    @Override
    public String getMileage() {

        return String.valueOf(this.milage) + " kmpl";
    }
}