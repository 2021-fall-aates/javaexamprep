package com.oreillyauto.problems.carInheritance;

class HondaCity extends Car{

    private int milage;
    private final static boolean isSedan = true;
    private final static String seats = "4";
    
    public HondaCity(int milage) {
        super(isSedan, seats);
        this.milage = milage;
    }

    public HondaCity(boolean isSedan, String seats, int milage) {
        super(isSedan, seats);
        this.milage = milage;
    }
    
    @Override
    public String getMileage() {
        
        return String.valueOf(this.milage) + " kmpl"; 
        }
    
}