package com.oreillyauto.problems.carInheritance;

class WagonR extends Car{
    
    private int milage;
    private final static boolean isSedan = false;
    private final static String seats = "4";
    
    
    public WagonR(int milage) {
        super(isSedan, seats);
        this.milage = milage;
    }

    @Override
    public String getMileage() {
               
        return String.valueOf(this.milage) + " kmpl";
    }
    
}