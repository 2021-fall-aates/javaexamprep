package com.oreillyauto.problems.compusa;

class Laptop {
    
    String sku;
    String serialNumber;
    int count = 0;
    public Laptop(String sku, String serialNumber) {
        super();
        this.sku = sku;
        this.serialNumber = serialNumber;
    }
    public String getSku() {
        return this.sku;
    }
    public String getSerialNumber() {
        return this.serialNumber;
    }
    
    public int getLaptopCount() {
        return count;
    }
    
    public int setLaptopCount(int num) {
        return count += num;
    }
    
    
}