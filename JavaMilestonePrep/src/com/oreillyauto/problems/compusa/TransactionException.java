package com.oreillyauto.problems.compusa;

class TransactionException extends Exception{
    
    String errorMessage;
    String errorCode;
    
    public TransactionException(String errorCode, String errorMessage) {
        super();
        
        this.errorMessage = errorMessage;
        this.errorCode = errorCode;
    }
    
    public String getErrorMessage() {
        return errorMessage;
    }
    public String getErrorCode() {
        return errorCode;
    }
    
}