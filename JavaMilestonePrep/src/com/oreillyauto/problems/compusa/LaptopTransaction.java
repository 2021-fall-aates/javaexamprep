package com.oreillyauto.problems.compusa;

// the error code is the enum, and the error message is the string
// I think there is a way to return the enum's name, and it's string for the exception class.

enum ErrorMessage {
    SERIAL_NUMBER_MISSING("Serial number Missing."),
    OUT_OF_STOCK("Out of stock."),
    INVALID_COUNT("Count should be greater than zero.");
    
    public final String errorMessage;
    
    ErrorMessage(String errorMessage){
        this.errorMessage = errorMessage;
    }
}

class LaptopTransaction{
    static String addLaptop(Laptop laptop, int count) throws TransactionException {
        String message = "";
        if (laptop.serialNumber == "" || laptop.serialNumber == null) {
             throw new TransactionException(String.valueOf(ErrorMessage.SERIAL_NUMBER_MISSING), ErrorMessage.SERIAL_NUMBER_MISSING.errorMessage);
             
        }
        if (count <= 0) {
            throw new TransactionException(String.valueOf(ErrorMessage.INVALID_COUNT), ErrorMessage.INVALID_COUNT.errorMessage);
        }
        
        if ((count > 0) && (laptop.serialNumber.length() > 0)) {
            laptop.setLaptopCount(count);
            message = "Laptops successfully added.";       
            }
        return message;
    }
    
    static String sellLaptop(Laptop laptop, int count) throws TransactionException {
        String message = "";
        
        if (count <= 0) {
            throw new TransactionException(String.valueOf(ErrorMessage.INVALID_COUNT), ErrorMessage.INVALID_COUNT.errorMessage);
        }
        else if(laptop.count < count) {
            throw new TransactionException(String.valueOf(ErrorMessage.OUT_OF_STOCK), ErrorMessage.OUT_OF_STOCK.errorMessage);
        }
        else {
            message = "Laptops successfully sold.";
        }
        return message;
    }
    
}