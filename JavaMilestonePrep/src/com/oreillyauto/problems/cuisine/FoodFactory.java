package com.oreillyauto.problems.cuisine;

import java.util.HashMap;
import java.util.Map;

class FoodFactory {
    static FoodFactory factory;
    
    Map<String, Cuisine> cuisineMap = new HashMap<String, Cuisine> ();
    
    public static FoodFactory getFactory() {
        if (factory == null) {
            factory = new FoodFactory();
        }
        return factory;
    }
    
    void registerCuisine(String cuisineKey, Cuisine cuisine) {
        cuisineMap.put(cuisineKey, cuisine);
    }
    
    Cuisine serveCuisine(String cuisineKey, String dish) throws UnservableCuisineRequestException{
        if (cuisineMap.containsKey(cuisineKey)) {
            return cuisineMap.get(cuisineKey).serveFood(dish);
        }
        else {
            throw new UnservableCuisineRequestException("Unservable cuisine " + cuisineKey + " for dish " + dish);
        }
    }
}