package com.oreillyauto.problems.cuisine;
abstract class Cuisine {
    public abstract Cuisine serveFood(String dish);
}