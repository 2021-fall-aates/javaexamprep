package com.oreillyauto.problems.cuisine;

class Chinese extends Cuisine{
    private String dish;
    
    public String getDish() {
        return dish;
    }
    @Override
    public Cuisine serveFood(String dish) {
        this.dish = dish;
        return this;
    }

}