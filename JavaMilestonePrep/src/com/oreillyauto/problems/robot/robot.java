package com.oreillyauto.problems.robot;

class Robot{
    Integer currentX;//  The robot's current x-coordinate in the 2D plane.
    Integer currentY;//  The robot's current y-coordinate in the 2D plane.
    Integer previousX;// The robot's x-coordinate in the 2D plane prior to its most recent movement.
    Integer previousY;// The robot's y-coordinate in the 2D plane prior to its most recent movement.
    String move;
    int moveAmt;
    
    public Robot() {
        this.currentX = 0;
        this.currentY = 5;
    }
    
    public Robot(Integer currentX, Integer currentY) {
        super();
        this.currentX = currentX;
        this.currentY = currentY;
    }
    
    void moveX(Integer dx) {
        this.setPreviousX(getCurrentX());
        this.setCurrentX((getCurrentX() + dx));
        this.setPreviousY(getCurrentY());
        this.setCurrentY(getCurrentY());
        this.move = "x";
        this.moveAmt = dx;
        
    }
    void moveY(Integer dy) {
        this.setPreviousX(getCurrentX());
        this.setCurrentX(getCurrentX());
        this.setPreviousY(getCurrentY());
        this.setCurrentY((getCurrentY() + dy));
        this.move = "y";
        this.moveAmt = dy;
    }
    
    String printCurrentCoordinates() {
        StringBuilder coordinates = new StringBuilder();
        coordinates.append(String.valueOf(getCurrentX()));
        coordinates.append(" ");
        coordinates.append(String.valueOf(getCurrentY()));
        return String.valueOf(coordinates);
    }
    
    String printLastCoordinates() {
        StringBuilder coordinates = new StringBuilder();
        coordinates.append(String.valueOf(getPreviousX()));
        coordinates.append(" ");
        coordinates.append(String.valueOf(getPreviousY()));
        return String.valueOf(coordinates);
    }
    
    String printLastMove() {
        StringBuilder thisMove = new StringBuilder();
        thisMove.append(this.move);
        thisMove.append(" ");
        thisMove.append(this.moveAmt);
        
        return String.valueOf(thisMove);
    }

    public Integer getCurrentX() {
        return currentX;
    }

    public void setCurrentX(Integer currentX) {
        this.currentX = currentX;
    }

    public Integer getCurrentY() {
        return currentY;
    }

    public void setCurrentY(Integer currentY) {
        this.currentY = currentY;
    }

    public Integer getPreviousX() {
        return previousX;
    }

    public void setPreviousX(Integer previousX) {
        this.previousX = previousX;
    }

    public Integer getPreviousY() {
        return previousY;
    }

    public void setPreviousY(Integer previousY) {
        this.previousY = previousY;
    }
}