package com.oreillyauto.problems.geometry;

public interface Square {
    public int area(int length);
}