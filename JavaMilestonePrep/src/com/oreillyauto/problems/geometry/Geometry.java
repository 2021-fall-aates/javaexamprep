package com.oreillyauto.problems.geometry;

class Geometry implements Square, Triangle {

    @Override
    public int area(int length) {
        // TODO Auto-generated method stub
        return (length * length);
    }

    @Override
    public int area(int base, int height) {
        // TODO Auto-generated method stub
        int trArea;
        if (base != 0) {
            trArea = (base/2) * height;
        } else {
            trArea = 0;
        }
        return trArea;
    }
    
}